<?php 
    // Deklarasi nilai default Variable
    $nama1 = $nama2 = $nama3 = $nama4 = $nama5 = "Belum Diisi";
    $umur1 = $umur2 = $umur3 = $umur4 = $umur5 = $berat1 = $berat2 = $berat3 = $berat4 = $berat5 = 0;
    $euclidean1 = $euclidean2 = $euclidean3 = $euclidean4 = 0;
    $echokesimpulan = $alertsuccess = $rumuseu1 = $rumuseu2 = $rumuseu3 = $rumuseu4 = $kemiripan = $alertsuccess = "";

    
    // Jika Button submit ditekan
    if ( isset ( $_POST['submit'] ) ) {
        

        // Looping untuk mengambil nilai dari form (method Post)
        $j=1;
        while ($j<=5) {
            ${'nama'.$j} = $_POST['nama'.$j];
            ${'umur'.$j} = $_POST['umur'.$j];
            ${'berat'.$j} = $_POST['berat'.$j];
            $j++;
        }
        
        // Rumus menghitung jarak euclidean
        $euclidean1 = round(sqrt(pow(($umur2-$umur1),2)+pow(($berat2-$berat1),2)),2);
        $euclidean2 = round(sqrt(pow(($umur3-$umur1),2)+pow(($berat3-$berat1),2)),2);
        $euclidean3 = round(sqrt(pow(($umur4-$umur1),2)+pow(($berat4-$berat1),2)),2);
        $euclidean4 = round(sqrt(pow(($umur5-$umur1),2)+pow(($berat5-$berat1),2)),2);

        // Cari nilai terkecil antar euclidean dengan dipindahkan ke dalam array
        $arr = array($euclidean1,$euclidean2,$euclidean3,$euclidean4);
        $terkecil = min($arr);
        
        // Penentuan yang paling mirip dari nilai terkecil
        if ($terkecil == $euclidean1) {
            $kemiripan = $nama2;
        }
        else if ($terkecil == $euclidean2) {
            $kemiripan = $nama3;
        }
        else if ($terkecil == $euclidean3) {
            $kemiripan = $nama4;
        }
        else if ($terkecil == $euclidean4) {
            $kemiripan = $nama5;
        }
        else {
            $kemiripan = "Kelimanya";
        }

        // Untuk Output
        // &emsp; = 4 Spasi
        // &#8730 = Simbol Akar
        // <sup> = Buat pangkat / superscript
        $rumuseu1 = "&emsp;($nama2, $nama1) = &#8730( ($umur2-$umur1)<sup>2</sup> + ($berat2-$berat1)<sup>2</sup> ) = $euclidean1";
        $rumuseu2 = "&emsp;($nama3, $nama1) = &#8730( ($umur3-$umur1)<sup>2</sup> + ($berat3-$berat1)<sup>2</sup> ) = $euclidean2";
        $rumuseu3 = "&emsp;($nama4, $nama1) = &#8730( ($umur4-$umur1)<sup>2</sup> + ($berat4-$berat1)<sup>2</sup> ) = $euclidean3";
        $rumuseu4 = "&emsp;($nama5, $nama1) = &#8730( ($umur5-$umur1)<sup>2</sup> + ($berat5-$berat1)<sup>2</sup> ) = $euclidean4";
        $echokesimpulan = "<h6>Jadi, $nama1 lebih mirip $kemiripan karena nilai euclideannya lebih kecil</h6>";
        
        // Jika proses input benar - Pemanggilan Sweet Alert 2
        $alertsuccess = "
        <script>
            Swal.fire(
                'Berhasil!',
                'Click tombol <strong>Tampil Hasil</strong> untuk melihat hasil perhitungan',
                'success'
            );
        </script>
        ";
    }

?>

<!DOCTYPE html>
<html lang="en">
<body>
    <script src="node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script>
</body>
</html>