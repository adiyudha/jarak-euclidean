<?php 
    require('logic.php');
?>

<!DOCTYPE html>
<html>
<head>
    <title>Perhitungan Jarak Euclidean</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
    <!-- ISIAN -->
    <div class="container">
    <h2 class="text-center mt-3 mb-3">Perhitungan Jarak Euclidean</h2>
    <figcaption class="blockquote-footer text-center">
        Developed by M Adiyudha - Satria Galang - Guntur Eki
    </figcaption>
    <?= $alertsuccess; ?>
    <form class="row" method="POST" enctype="multipart/form-data" action="">

        <!-- Looping untuk 5 orang dengan atribut yang sama -->
        <?php 
            for($i=1; $i<=5; $i++){
        ?>
                <div class="mb-3 col-md-4">
                    <label for="nama<?= $i; ?>" class="form-label">Nama Orang <?= $i; ?></label>
                    <input type="text" class="form-control" name="nama<?= $i; ?>" required>
                </div>
                <div class="mb-3 col-md-4">
                    <label for="umur<?= $i; ?>" class="form-label">Umur</label>
                    <div class="input-group">
                        <input type="number" class="form-control" name="umur<?= $i; ?>" required>
                        <span class="input-group-text" name="addon-umur<?= $i; ?>">Tahun</span>
                    </div>
                </div>
                <div class="mb-3 col-md-4">
                    <label for="berat<?= $i; ?>" class="form-label">Berat</label>
                    <div class="input-group">
                        <input type="number" class="form-control" name="berat<?= $i; ?>" required>
                        <span class="input-group-text" name="addon-berat<?= $i; ?>">Kg</span>
                    </div>
                </div>

        <?php }; ?>
        <!-- Looping END -->


        <div class="d-grid gap-2 col-md-4 mx-auto mb-3">
            <button type="submit" name="submit" class="btn btn-primary" value="Hitung" >Submit</button>
            <button type="button" name="tampil" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Tampilkan Hasil</button>
        </div>
    </form>
    
    </div>
    <!-- ISIAN END -->


    <!-- Tampil Data dengan Modal Bootstrap / Popup-->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Hasil Perhitungan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            <table class="table text-center">
                    <thead>
                        <tr class="table-dark">
                            <th scope="col">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Umur</th>
                            <th scope="col">Berat Badan</th>
                            <th scope="col">Peran</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td><?= $nama1; ?></td>
                            <td><?= $umur1; ?></td>
                            <td><?= $berat1; ?></td>
                            <td></td>
                        </tr>

                        <!-- Loop untuk membuat row 2 hingga 5 -->
                        <?php 
                            for($r=2; $r<=5; $r++){
                        ?>
                        <tr>
                            <th scope="row"><?= $r; ?></th>
                            <td><?= ${"nama".$r}; ?></td>
                            <td><?= ${"umur".$r}; ?></td>
                            <td><?= ${"berat".$r}; ?></td>
                            <td>Pembanding <?= $r-1; ?></td>
                        </tr>
                        <?php 
                            };
                        ?>
                    </tbody>
                </table>

                <h5>Rumus Jarak Euclidean</h5>
                <p>
                    <?= $rumuseu1; ?> <br> 
                    <?= $rumuseu2; ?> <br> 
                    <?= $rumuseu3; ?> <br> 
                    <?= $rumuseu4; ?> <br> 
                    <br> <?= $echokesimpulan; ?>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Mengerti</button>
            </div>
            </div>
        </div>
    </div>
    <!-- Tampil Data END -->

    <!-- Pemanggilan framework javascript -->
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="node_modules/sweetalert2/dist/sweetalert2.all.min.js"></script>
</body>
</html>